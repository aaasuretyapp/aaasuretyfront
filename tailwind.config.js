module.exports = {
  theme: {
    maxHeight: {
      '0': '0',
      '1/4': '25vh',
      '1/2': '50vh',
      '2/3': '66vh',
      '6/10': '60vh',
      '3/4': '75vh',
      'full': '100%',
    },
    extend: {
      transitionDuration: {
        '2000': '2000ms',
        '3000': '3000ms',
      },
      transitionTimingFunction: {
        'in-expo': 'cubic-bezier(0.95, 0.05, 0.795, 0.035)',
        'out-expo': 'cubic-bezier(0.19, 1, 0.22, 1)',
      },
      height: {
        '1/2': '50vh'
      },
      maxWidth: {
        '8xl': '80rem',
        '1/4' : '25%',
        '300' : '300px'
      },
      fontFamily: {
        sans: ['Inter var'],
      },
    }
  },
  variants: { // all the following default to ['responsive']
    spinner: ['responsive'],
  }, plugins: [
  ]
}
