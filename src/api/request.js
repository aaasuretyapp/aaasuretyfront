import axios from 'axios';

const instance = axios.create({
	baseURL: 'https://surey-wld.ue.r.appspot.com',
	timeout: 30000,
	https: true,
});

export default instance;