function translateError(errorCode) {
  if (errorCode === "auth/invalid-email") {
    return("Formato de email incorrecto");
  }
  if (errorCode === "auth/user-not-found") {
    return("Usuario no encontrado");
  }
  if (errorCode === "auth/wrong-password") {
    return("Contrasena incorrecta");
  }
  if (errorCode === "auth/too-many-requests") {
    return("Demasiados intentos, intenta mas tarde");
  }
}

export default translateError;