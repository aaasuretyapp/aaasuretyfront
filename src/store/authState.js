import { store } from '@risingstack/react-easy-state';

const authState = store({
    currentUser: null,
    setCurrentUser: (user) => authState.currentUser = user,
  });

export default authState;